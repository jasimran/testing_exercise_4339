require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  setup do
    @account = Account.new
  end

  test "An account with no deposits should return 0 for its amount" do
    assert_equal 0,@account.balance
  end

  test "An account with one deposit should have a balance equal to that deposit" do
    @account.deposit_amount(10)
    assert_equal 10, @account.balance
  end



  test "Withdrawing from the account should alter the balance" do
    @account.deposit_amount(100)
    assert @account.withdraw_amount(10)
    assert_equal 90, @account.balance
  end

  test "A user cannot withdraw more than what is in the account" do
    @account.deposit_amount(10)
    assert  !@account.withdraw_amount(100), "Withdrawal incorrectly allowed"
    assert_equal 10, @account.balance
  end

  test "Deposits cannot be negative numbers" do
    assert_raise (RuntimeError) {@account.deposit_negative?(-3)}
  end

  test "Negative deposits should be ignored" do
    @account.deposit_amount(-3)
    assert_equal 0, @account.balance
  end

  test "Withdrawals must be positive" do
    assert_raise (RuntimeError) {@account.withdraw_amount(-3)}
  end

  test "Add interest for balance greater than 200" do
    @account.deposit_amount(210)
    assert_equal 220, @account.balance
  end

  test "Incur low balance fee" do
    @account.deposit_amount(30)
    assert  @account.withdraw_amount(10), "No low balance fee incurred"
    assert_equal 15, @account.balance
  end

  test "Change account balance to zero for account with less than 5" do
    @account.deposit_amount(30)
    assert  @account.withdraw_amount(27), "Account balance not changed to 0 for balance less than 5"
    assert_equal 0, @account.balance
  end
end
