class Account < ActiveRecord::Base

  after_initialize :init_balance


  def deposit_amount(amount)
    #increase balance if amount is not negative
    if amount >= 0
      self.balance += amount
      #add interest if balance greater than 200
      if self.balance >200
        self.balance += 10
      end
      #ignore if deposit is negative
    elsif amount < 0
      self.balance = self.balance
    else
      false
    end
  end

  def deposit_negative?(amt)
    if amt < 0
      #deposits cannot be negative, raise error
      raise RuntimeError
    end
  end

  def withdraw_amount(amount)
    if amount < self.balance && amount >= 0
      self.balance -= amount
      new_bal = self.balance
      #charge low balance fee
      if new_bal < 25 && new_bal >= 5
        self.balance -= 5
      #turn balance to zero if balance is less than 5
      elsif new_bal < 5
        self.balance = 0
      else
        self.balance = self.balance
      end
    #withdrawals cannot be negative
    elsif amount < 0
        raise RuntimeError
    else
      false
    end


  end



  private

  def init_balance
    self.balance ||= 0
  end
end

